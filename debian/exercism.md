% exercism(1) exercism Manual
% Mike Gehard, Katrina Owen
% 2019-04-29
# NAME

exercism - commandline interface for *http://exercism.io* website

# SYNOPSIS

**exercism** [*globalflags*] *command* [*args*]

# DESCRIPTION

## Available commands:

configure [*flags*]
:     Configure the command-line client to customize it to your needs.
      This lets you set up the CLI to talk to the API on your behalf,
      and tells the CLI about your setup so it puts things in the right
      places. Following flags are known:

      -a, \--api *string*
      : API base url

      \--no-verify
      : skip online token authorization check

      -s, \--show
      : show the current configuration

      -t, \--token *string*
      : authentication token used to connect to the site

      -w, \--workspace *string*
      : directory for exercism exercises

      -h, \--help
      : help for configure

download [*flags*]
:    Download an exercise.

     You may download an exercise to work on. If you've already started
     working on it, the command will also download your latest solution.
     Download other people's solutions by providing the UUID. Following
     flags are known:

     -e, \--exercise *string*
     : the exercise slug

     -T, \--team *string*
     : the team slug

     -t, \--track *string*
     : the track ID

     -u, \--uuid *string*
     : the solution UUID

     -h, \--help
     : help for download

open
: Open the exercise in current directory on the Exercism website with
  web browser.

prepare 
: Prepare downloads settings and dependencies for Exercism and the
  language tracks.

submit *file* [*file* *...*]
: Submit your solution to an Exercism exercise.

troubleshoot
: Provides output to help with troubleshooting. If you're running into
  trouble, copy and paste the output from the troubleshoot command into a
  GitHub issue so we can help figure out what's going on.

version
: Output the version of the exercism binary that is in use.

workspace
: Print out the path to your Exercism workspace. This command can be used
  for scripting, or it can be combined with shell commands to take you to
  your workspace.

## Global flags

--timeout int
: override the default HTTP timeout (seconds)

-v, --verbose
: verbose output
